#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    //int's
    int trys = 0;
    int guess;
    int points = 0;
    int number;
    int again = 1;
    int count = 1;
    int playagain = 0;
    srand(time(NULL));

    //initial print statements to greet player
    printf("Welcome to the number guessing game!!!\n");
    printf("\nFor every correct guess you will gain a point.\n");
    printf("\nYou will have 5 tries to guess the correct number.\n\n");

    while(again!=0)
    {
        
        //where the random number is generated
        number=rand()%20 + 1;
        printf("Guess a number between 1 and 20:  ");
        scanf("%i", &guess);
        trys=trys+1;

        while (playagain=0 || trys<5)
        {
            if (guess<1 || guess>20)
            {
                //if a number that is not between 1 and 20 is inputed
                printf("Invalid input! Enter a value between 1 and 20:  ");
                scanf("%i", &guess);
            }
            else if(guess>number)
            {
                //If the number is incorrect and high, the user will get this message
                printf("\nGuess Lower:  ");
                scanf("%i", &guess);
                trys=trys+1;

                if (trys==5 && guess==number)
                {
                    //If the number is correct they get this message
                    printf("\nGood Job! You earned a point.\n");
                    points=points+1;
                    printf("You have %i points!\n",points);
                }
            }
            else if (guess<number)
            {
                //if the number is too low and incorrect they get this message
                printf("\nGuess Higher:  ");
                scanf("%i", &guess);
                trys=trys+1;

                if (trys==5 && guess==number)
                {
                    printf("\nGood Job! You earned a point.\n");
                    points=points+1;
                    printf("You have %i points!\n",points);
                }
            }
            else if(guess==number)
            {
                trys=5;
                printf("\nGood job! You earned a point.\n");
                points=points+1;
                printf("You have %i points!\n",points);
            }

        }
        if (trys==5 && guess!= number)
        {
            //what the player gets after 5 tries
            printf("\nSorry, the number was %i.\n",number);
            printf("You have %i total points.\n\n",points);
        }

        //if the player wants to go again
        printf("\nWould you like to play again? (1 for yes, 0 for no)  ");
        scanf("%i", &again);
        scanf("%i", &playagain);

        while(count==1 || playagain++)
        {
            if(again>1 || again<0)
            {
                printf("Invalid input! Enter a 1 for yes or a 0 for no:  ");
                scanf("%i", &again);
               
            }
            else if(again==1 || again==0)
            {
                count=0;
                playagain=0;
            }
        }
        printf("\n\n\n\n");
    }

return 0;
}



