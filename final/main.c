#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int NumSecret, Guess;
    srand(time(NULL));
    NumSecret = rand() % 20 + 1;
    int tries = 0;

    printf("\nWelcome to the number guessing game!\n\nYou will have 5 chances to guess a number from 1 to 20.\n\n");


    for (tries = 0; tries < 9 || NumSecret == Guess; tries++)
    {
        printf("Please, enter a number between 1 and 20!\n\n");
        scanf("%d", &Guess);

        if (Guess == NumSecret)
        {
            printf("\nCongratulations! You won!");
            return 0;
        }

        else if (Guess > 20)
        {
            printf("\nYou entered an invalid number.\n");
        }

        else if (Guess > NumSecret)
        {
            tries++;
            printf("\nYour guess was too high.\n");
        }

        else if (Guess < NumSecret)
        {
            tries++;
            printf("\nYour guess was too low.\n");
        }
        if (tries == 9)
            break;

    }

    printf("\nYou have used all your chances. The correct number is %d.\n", NumSecret);

    return 0;
}
